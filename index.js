// erquire director tells us to load the express module
const express = require('express')



// creating a server using express
const app = express();

//port
const port = 4000;

// middlewares
app.use(express.json())
	// allows app to read a json data
app.use(express.urlencoded({extended: true}));
	// allows app to read data from forms
	// by default, information received from the url can only be received as string or an array
	// with extended: true, this allows to receive information in other data types such as objects.

// mock database

let users = [
	{
	email: "nezukoKamado@gmail.com",
	username: "nezuko01",
	password: "letMeout",
	isAdmin: false
	},
	{
		email: "zenitsuAgatsuma@gmail.com",
		username: "zenitsuSleeps",
		password: "iNeedNezuko",
		isAdmin: true
	}
]

let loggedUser;

// app.get('/', (req, res) => {
// 	res.send('Hello World')
// });

// app.get('/', (req, res) => {
// 	res.send(users)
// });

app.get('/hello', (req, res) => {
	res.send('hello from batch 131')
});

// app.post('/', (req, res) => {
// 	console.log(req.body)
	
// 	res.send('hello im post method')

// })

app.post('/', (req, res) => {
	console.log(req.body)
	let body2 = req.body
	res.send(`hello im ${body2.age}`)

})

// use console.log to check what's inside the request body.

// solution: ('hello i am ${body2.age}`)

// register route

app.post('/users/register', (req, res) => {
	console.log(req.body);

	let newUser = {
		email: req.body.email,
		username: req.body.username,
		password: req.body.password,
		isAdmin: req.body.isAdmin
	}
	users.push(newUser);
	console.log(users);

	res.send(`User ${req.body.username} has successfully register.`)
})

// login route

app.post('/users/login', (req, res) => {
	console.log(req.body);

	let foundUser = users.find((user) => {
		return user.username === req.body.username && user.password === req.body.password;
	});

	if(foundUser !== undefined) {

		let foundUserIndex = users.findIndex((user) => {

			return user.name === foundUser.username
		});
		foundUser.index = foundUserIndex;

		loggedUser = foundUser

		console.log(loggedUser)

		res.send('Thank you for logging in.')
	} else {
		loggedUser = foundUser;
		res.send('Login Failed, wrong credentials.')
	}
})

// app - server
// get - http method
// '/' - route name or endpoint
// (req,rest) - request and response - will h andle the request and the responses
// res.send - combines writehead() and end(), used to send response to our client


// listen to the port and returning the message in the terminal. 



// Change-Password Route

app.put('/users/change-password', (req, res) => {

	// store the message
	let message;

	// will loop through all the users array
	for(let i = 0; i < users.length; i++){

		// if the username provided in the request is the same with the username in hte loop
		if(req.body.username === users[i].username){

			// change the password of the user found in the loop by the requested password in the body by the client
			users[i].password = req.body.password;

			// send a message to the client
			message = `User ${req.body.username}'s password has been changed.`;
			// break the loop once a usesr matchees the username provided in the clilent
			break;
		// if no useer was foound
		} else {
			// changees the message to be send aas a response
			message = `User not found.`
		}
	}

	res.send(message)
})


/*

ACTIVITY

*/


// 1

app.get('/home', (req, res) => {
	res.send('Hello, welcome to home page')
});

// 2

app.get('/users', (req, res) => {
		res.send(users)
	});


//5 

app.delete('/delete-user', (req, res) => {

	
	let message;

	
	for(let i = 0; i < users.length; i++){

		
		if(req.body.username === users[i].username){

			
			users[i].username = req.body.username;

			
			message = `User ${req.body.username} has been deleted.`;
			
			break;
		d
		} else {
			
			message = `User not found.`
		}
	}

	res.send(message)
})

app.listen(port, () => console.log(`The Server is running at port ${port}`));